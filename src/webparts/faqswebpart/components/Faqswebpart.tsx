import * as React from 'react';
import { SPFI } from '@pnp/sp';
// import styles from './Faqswebpart.module.scss';
import type { IFaqswebpartProps } from './IFaqswebpartProps';
// import { escape } from '@microsoft/sp-lodash-subset';

import { getSP } from '../../../pnpjsConfig';
import { FAQ } from '../../../interfaces';
import { Accordion, Placeholder, WebPartTitle } from '@pnp/spfx-controls-react';

const Faqswebpart: React.FC<IFaqswebpartProps> = props => {
    const {
        context,
        listGuid,
        displayMode,
        title,
        updateProperty
        // description,
        // isDarkTheme,
        // environmentMessage,
        // hasTeamsContext,
        // userDisplayName
    } = props;

    const _sp: SPFI = getSP(context)

    const [faqs, setFaqs] = React.useState<FAQ[]>([])

    const getFAQs = async (): Promise<void> => {
        const res = await _sp.web.lists.getByTitle("FAQ").items.orderBy('Letter', true).orderBy('Title', true)()
        
        setFaqs(res.map(itm => ({ 
            Id: itm.Id, 
            Title: itm.Title, 
            Body: itm.Body, 
            Letter: itm.Letter 
        })) )
    }

    React.useEffect(() => {
        if (listGuid && listGuid !== '')
            getFAQs()
    }, [props])

    return (
        <div>
            <WebPartTitle displayMode={displayMode}
                title={title}
                updateProperty={updateProperty} />
            <h1>Karibu (Welcome to) Kenya.</h1>
            {
                props.listGuid !== '' ?
                    faqs.map((itm: FAQ, index: number) => (
                        <Accordion key={index} title={itm.Title} defaultCollapsed={true}>
                            {itm.Body}
                        </Accordion>
                    ))
                :
                    <Placeholder iconName='Edit'
                        iconText='Configure your web part'
                        description='Please configure the web part.'
                        buttonLabel='Configure'
                        onConfigure={props.context.propertyPane.open} />
            }
        </div>
    )
}

export default Faqswebpart