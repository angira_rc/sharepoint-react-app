import { DisplayMode } from "@microsoft/sp-core-library";
import { WebPartContext } from "@microsoft/sp-webpart-base";

export interface IFaqswebpartProps {
  context: WebPartContext;
  title: string;
  displayMode: DisplayMode;
  updateProperty: (value: string) => void;
  listGuid: string;
  description: string;
  isDarkTheme: boolean;
  environmentMessage: string;
  hasTeamsContext: boolean;
  userDisplayName: string;
}
