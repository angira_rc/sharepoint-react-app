export interface FAQ {
    Id: number;
    Title: string;
    Body: string;
    Letter: string;
}

export interface FAQState {
    faqItems: FAQ[];
}

export interface IPropertyControlsTestWebPartProps {
    lists: string | string[]; // Stores the list ID(s)
}

